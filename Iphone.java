public class Iphone{
	private int numberOfContacts;
	private int numberOfSongs;
	private String yourUsername;
	
	public void appleMusic(){
		if(this.numberOfSongs > 30){
			System.out.println("You listen to music a lot since you have more than 30 songs!");
		}else if(this.numberOfSongs <= 30 && this.numberOfSongs >= 10){
			System.out.println("You do listen to music sometimes since you have 10 to 30 songs!");
		}else{
			System.out.println("You listen to music very rarely since you have less than 10 songs!");	
		}
	}
	//getter methods
	public int getNumberOfContacts(){
		return this.numberOfContacts;
	}
	public int getNumberOfSongs(){
		return this.numberOfSongs;
	}
	public String getYourUsername(){
		return this.yourUsername;
	}
	
	//setter methods
	public void setNumberOfContacts(int newNumberOfContacts){
		this.numberOfContacts = newNumberOfContacts;
	}
	public void setNumberOfSongs(int newNumberOfSongs){
		this.numberOfSongs = newNumberOfSongs;
	}
	/*public void setYourUsername(String newYourUsername){
		this.yourUsername = newYourUsername;
	}*/
	
	//Constructor
	public Iphone(int numberOfSongs, int numberOfContacts, String yourUsername){
		this.numberOfContacts = numberOfContacts;
		this.numberOfSongs = numberOfSongs;
		this.yourUsername = yourUsername;
	}
}