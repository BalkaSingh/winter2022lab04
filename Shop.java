import java.util.Scanner;
public class Shop{
	public static void main(String[] args){
	Scanner sc = new Scanner(System.in);
	//array with 4 Iphone	
	Iphone[] product = new Iphone[4];
	
	for(int i = 0;i< product.length; i++){
		
		//New Iphone message 
		System.out.println("");
		System.out.println("---New Iphone---");
		System.out.println("Enter the number of contacts in your Iphone: ");
		int numberOfContacts = sc.nextInt();
		System.out.println("Enter the number of songs in your Iphone: ");
		int numberOfSongs = sc.nextInt();
		System.out.println("Enter your username for the Iphone(one word): ");
		String yourUsername = sc.next();
		System.out.println("");	
		
		product[i] = new Iphone(numberOfContacts, numberOfSongs,yourUsername);		
	} 
	//Last Iphone fields before updating information
	System.out.println("The number of contacts in the last Iphone before update: " +product[product.length - 1].getNumberOfContacts());
	System.out.println("The number of songs in the last Iphone before update: " +product[product.length - 1].getNumberOfSongs());
	System.out.println("The username in the last Iphone before update: " +product[product.length - 1].getYourUsername());
	
	//Update the fields of the last Iphone
	System.out.println("");
	System.out.println("---Last Iphone Update : New Information---");
	System.out.println("Enter the number of contacts for the last Iphone: ");
	product[product.length - 1].setNumberOfContacts(sc.nextInt());
	System.out.println("Enter the number of songs for the last Iphone: ");
	product[product.length - 1].setNumberOfSongs(sc.nextInt());
	System.out.println("");
	
	//Last Iphone field after updating information
	System.out.println("The number of contacts in the last Iphone after update: " +product[product.length - 1].getNumberOfContacts());
	System.out.println("The number of songs in the last Iphone after update: " +product[product.length - 1].getNumberOfSongs());
	System.out.println("The username in the last Iphone after update: " +product[product.length - 1].getYourUsername());
	System.out.println("");
	
	//Calling the instance method appleMusic on the last Iphone(product)
	product[product.length - 1].appleMusic();
	}
}